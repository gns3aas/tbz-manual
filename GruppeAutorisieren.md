# Gruppe Autorisieren

 - Stand: 04.03.2024
 - Zeitaufwand: Max. 15 Minuten

## Einleitung

Bevor eine Benutzer\*in bzw. eine Lernende*r eine GNS3 Session unter https://cloud.edu.tbz.ch/gns3/ starten kann, muss der die Gruppe bzw. Klasse der Benutzer\*in autorisiert werden. 

Die Freigabe kann die Lehrperson selbstständig mithilfe des CLI Tools `GNS3aaS.CLI.exe` vornehmen. 

## Installation - Windows
 
 - Die aktuelle CLI Version kann unter https://gitlab.com/gns3aas/backend/-/releases heruntergeladen werden (`cli.zip`)
![cli.zip](./media/clizip.JPG)
 - *Hinweis: Die Datei, welche Heruntergeladen wird heisst `artifacts.zip`*
 - In der Datei befinden sich für unterschiedliche Betriebssysteme kompilierte ausführbare Dateien. Im Zip befindet sich der Ordner `cli-win-x64` in welchem die EXE-Datei `GNS3aaS.CLI.exe`
 - Es empfiehlt sich diese EXE-Datei im User-Order unter Programme abzulegen. z.B. `C:\Users\user\Programme\`. 

## Gruppe aktivieren
 - PowerShell öffnen und in das Verzeichnis wechseln, in der das EXE abgelegt ist. 
![Login](./media/login.png)
 - Ein Token via E-Mail verlangen mit `.\GNS3aaS.CLI.exe login https://cloud.edu.tbz.ch vorname.nachname@tbz.ch`.
 - Der Token aus dem E-Mail kopieren (Alles nach `...dashboard?t=`) und an den Befehl `.\GNS3aaS.CLI.exe token` anfügen.
![Token](./media/token.png)
 - Mit `.\GNS3aaS.CLI.exe group list` die Liste der aktiven Gruppen anzeigen lassen.
![Gruppen](./media/groups.png)
 - Mit `.\GNS3aaS.CLI.exe group activate KLASSE vorname.nachname@tbz.ch` (`KLASSE` durch die gewünschte Klasse ersetzen und `vorname.nachname@tbz.ch` durch die E-Mail der verantwortlichen Lehrperson) kann die Klasse aktiviert werden. Wenn eine [UUID](https://de.wikipedia.org/wiki/Universally_Unique_Identifier) zurückgegeben wird, war es erfolgreich. 
 - Mit `.\GNS3aaS.CLI.exe group list` kann zusätzlich überprüft werden, ob die Aktivierung erfolgreich war. 



